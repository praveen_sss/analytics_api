const express = require("express");
const mysql = require("mysql")
const app = express();
const port = 3000;
const cors = require("cors");
app.use(cors());
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);


const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'analytics',
  multipleStatements: true
})

db.connect((err) => {
  if (err) {
    throw err;
  }
  console.log("Mysql Connected")
})


app.get("/showTables", (req, res) => {
  let sql = "SHOW TABLES FROM analytics "
  let query = db.query(sql, (err, result) => {
    if (err) throw err;
    console.log(JSON.parse(JSON.stringify(result)))
    res.send(JSON.parse(JSON.stringify(result)))
  });

})


app.get("/showColumns", (req, res) => {
  let sql = `Select * from ${req.query.tname} `
  let query = db.query(sql, (err, result) => {
    if (err) throw err;
    console.log(JSON.parse(JSON.stringify(result)))
    res.send(JSON.parse(JSON.stringify(result)))
  });

})
app.get("/getData", (req, res) => {

  db.query('SELECT * from chartdata; SELECT * from dashboard; select * from position ;select * from chartprops; select label,value from chartvalue ', [1, 2, 3, 4, 5], function (err, results) {
    if (err) throw err;

    console.log(results, "....");

    var dashboardId = (JSON.stringify(results[1].id))
    var bgColor = (results[1][0].bgColor)
    var chartdatas = (JSON.parse(JSON.stringify(results[0])))
    // var chartdata = (JSON.parse(JSON.stringify(chartdatas).replace(/[[\]]/g, '')))
    var p1 = JSON.parse(JSON.stringify(results[2]))
    var chartPropss = (JSON.parse(JSON.stringify(results[3])))
    var chartprops = (JSON.parse(JSON.stringify(chartPropss).replace(/[[\]]/g, '')))
    var chartvalue = (JSON.parse(JSON.stringify(results[4])))



    var layout = []
    chartdatas.map((e) => {
      var layou = {
        position: (JSON.parse(JSON.stringify(p1).replace(/[[\]]/g, ''))),
        chartdata: { ...JSON.parse(JSON.stringify(e).replace(/[[\]]/g, '')), datasource: { chartprops, data: chartvalue } },
        chartId: JSON.parse(JSON.stringify(results[2][0].chartId))
      }
      layout.push(
        JSON.stringify(layou)
      )
    }


    )

    console.log(layout)
    var finalData = {
      dashboardId,
      bgColor,
      layout,
    }
    console.log(finalData, "finalData")
    res.json(finalData)
  });
});



app.post("/addValue", (req, res) => {
  let newValue = req.body
  let sql = 'Insert INTO chartvalue SET ?';
  let query = db.query(sql, newValue, (err, result) => {
    if (err) throw err;
    console.log(result)
    res.send('Value Added Successfully')
  });
})


app.post("/addChart", (req, res) => {
  let newValue = req.body
  let sql = 'Insert INTO chartdata SET ?';
  let query = db.query(sql, newValue, (err, result) => {
    if (err) throw err;
    console.log(result)
    res.send('Chart Added Successfully')
  });
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});